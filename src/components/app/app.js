import React from 'react';
import './app.css';
import Nav from '../navigation/nav'
import ShotsList from '../shotsList/shotsList'
import LoginPage from '../loginPage/loginPage'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


function App() {
    return (
        <Router>
            <div className="App">
                <Nav/>
                <Switch>
                    <Route path='/' exact component={Home}/>
                    <Route path='/login' component={LoginPage}/>
                    <Route path='/shots' component={ShotsList}/>
                </Switch>
            </div>
        </Router>
    );
}

function Home() {
    return (
        <div>
            <h1>Home page</h1>
        </div>
    )
}

export default App;


