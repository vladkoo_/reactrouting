import React from 'react'
import './nav.css';
import {Link} from 'react-router-dom';

function Nav() {
    return (
        <div className="navbar">
            <Link to={'/'}>
                <span>Home</span>
            </Link>
            <Link to={'shots'}>
            <span>All shots</span>
            </Link>
            <Link to={'login'}>
            <span>Log IN</span>
            </Link>
        </div>
    )
}

export default Nav;
