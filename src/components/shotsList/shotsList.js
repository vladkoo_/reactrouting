import React, {useEffect, useState} from 'react'
import './shotsList.css';
import axios from 'axios';


function ShotsList() {

    const [shots, setShots] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:4000/shot/list')
            .then((shots) => {
                //setShots(shots);
                console.log(shots);
            })
            .catch((error)=>{
                console.log(error)
            });
    }, []);

    return (
        <div className="shotList">
            <h1>Shots List</h1>
            {shots}
        </div>
    )
}

export default ShotsList;
